import React from 'react';

class Check extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            checked: false,
        };
    }

    render() {
        let message = this.state.checked ? 'Checked' : 'No check';

        return (
            <div>
                <input type="checkbox" defaultChecked={ this.state.checked } onChange={ this.onCheck.bind(this) }/>
                <p>Checkbox { message }</p>
            </div>
        );
    }

    onCheck() {
        this.setState({
            checked: !this.state.checked,
        });
    }
}

export default Check;