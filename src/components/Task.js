import React from 'react';

export class Task extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            edit: false,
        };
    }

    render() {
        return this.state.edit ? this.renderEditable() : this.renderDefault();
    }

    renderDefault() {
        return (
            <div className="task">
                <div className="task__text">{ this.props.children }</div>
                <div className="task__buttons">
                    <button className="task__button" onClick={ this.edit.bind(this) }>Edit</button>
                    <button className="task__button task__button--danger" onClick={ this.remove.bind(this) }>Delete</button>
                </div>
            </div>
        );
    }

    renderEditable() {
        return (
            <div className="task">
                <input type="text" ref="taskText" className="task__input" defaultValue={ this.props.children }/>
                <div className="task__buttons">
                    <button className="task__button task__button--success" onClick={ this.save.bind(this) }>Save</button>
                </div>
            </div>
        );
    }

    edit() {
        this.setState({
            edit: true,
        });
    }

    remove() {
        this.props.remove(this.props.index);
    }

    save() {
        let value = this.refs.taskText.value;

        this.setState({
            edit: false,
        });

        this.props.update(value, this.props.index);
    }
}