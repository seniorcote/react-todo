import React from 'react';
import { Task } from 'Components/Task';

export class TaskList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            tasks: [],
        };
    }

    render() {
        return (
            <div className="tasks">
                {
                    this.state.tasks.map((task, i) => {
                        return (<Task key={ i } index={ i } update={ this.updateTask.bind(this) } remove={ this.removeTask.bind(this) }>{ task }</Task>);
                    })
                }
                <div className="tasks__create-button" onClick={ this.createTask.bind(this) }>
                    <span>Create new task</span>
                </div>
            </div>
        );
    }

    createTask(text) {
        let tasks = this.state.tasks;

        tasks.push('');

        this.setState({
            tasks: tasks,
        });
    }

    updateTask(text, i) {
        let tasks = this.state.tasks;

        tasks[i] = text;

        this.setState({
            tasks: tasks,
        });
    }

    removeTask(i) {
        let tasks = this.state.tasks;

        tasks.splice(i, 1);

        this.setState({
            tasks: tasks,
        });
    }
}