import React from 'react';
import ReactDOM from 'react-dom';
import { TaskList } from 'Components/TaskList';
import 'Styles/style';

ReactDOM.render(
    <TaskList/>,
    document.getElementById('app')
);