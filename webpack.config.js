const path = require('path');
const UglifyJsWebpackPlugin = require('uglifyjs-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname),
        filename: 'bundle.js',
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: [
                    'babel-loader',
                ],
            },
            {
                test: /\.less$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                minimize: true,
                            },
                        },
                        {
                            loader: 'less-loader',
                        },
                    ],
                }),
            },
        ],
    },
    resolve: {
        extensions: ['*', '.js', '.jsx', '.less'],
        alias: {
            Styles: path.resolve(__dirname, 'src/less/'),
            Components: path.resolve(__dirname, 'src/components/'),
        },
    },
    optimization: {
        minimizer: [
            new UglifyJsWebpackPlugin({
                sourceMap: false,
                exclude: [/\.min\.js$/gi],
                uglifyOptions: {
                    sourceMap: true,
                    compress: {
                        drop_console: true,
                        conditionals: true,
                        unused: true,
                        comparisons: true,
                        dead_code: true,
                        if_return: true,
                        join_vars: true,
                        warnings: false,
                    },
                    output: {
                        comments: false,
                    },
                },
            }),
        ],
        noEmitOnErrors: true,
        removeAvailableModules: true,
        removeEmptyChunks: true,
        mergeDuplicateChunks: true,
        flagIncludedChunks: true,
        occurrenceOrder: true,
        providedExports: true,
        usedExports: true,
        concatenateModules: true,
        sideEffects: true,
        minimize: true,
    },
    plugins: [
        new ExtractTextPlugin('style.css', { allChunks: false }),
    ],
};